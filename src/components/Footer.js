import React, {Component} from 'react'
import './Footer.css'
import { Link } from 'react-router-dom'

export default class Footer extends Component {
  render () {
    return (
      <footer className="footer">
        <ul>
          <li className="footer__link footer__link--dashboard">
            <Link to="/">
              <span>Dashboard</span>
            </Link>
          </li>
          <li className="footer__link footer__link--schedule">
            <a target="_blank" href="https://www.icloud.com/calendar/share/#st=e&i=2_GEYTOMBSGQ3DKNBWGUYTCNZQGJCVBZHYWHTMVOLO6FD372QEXTKOU7UGA6UAIFUT6INUFDGE23RV4" rel="noopener noreferrer">
              <span>Schedule</span>
            </a>
          </li>
          <li className="footer__link footer__link--trophy-case">
            <Link to="/prize-pond">
              <span>Prize Pond</span>
            </Link>
          </li>
          <li className="footer__link footer__link--gym-routine">
            <a target="_black" href="http://13.70.181.152/en/dashboard" rel="noopener noreferrer">
              <span>Gym Routine</span>
            </a>
          </li>
          <li className="footer__link footer__link--notepad">
            <Link to="/notepad">
              <span>Notepad</span>
            </Link>
          </li>
        </ul>
      </footer>
    )
  }
}
