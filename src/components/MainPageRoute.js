import React, {Component} from 'react'
import { Route } from 'react-router-dom'
import Header from './Header'

const MainPageRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => (
    <div>
      <Header/>

      <Component {...props}/>
    </div>
  )}/>
)

export default MainPageRoute
