import React, {Component} from 'react'
import CountUp from 'react-countup';

const Counter = ({end}) => {
  return <CountUp
    start={0}
    decimals={1}
    end={end}
    duration={1}
  />
}

export default Counter
