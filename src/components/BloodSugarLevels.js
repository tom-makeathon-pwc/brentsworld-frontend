import React, {Component} from 'react'
import './BloodSugarLevels.css'
import Counter from './Counter'
import { getSugarLevels } from '../services/index'
import {sortBy} from 'lodash'

export default class BloodSugarLevels extends Component {
  constructor (props) {
    super(props)

    this.state = {
      morningSugarReading: 0,
      afternoonSugarReading: 0,
    }
  }

  componentDidMount() {
    getSugarLevels().then(sugarLevels => {
      const sortedSugarLevels = sortBy(sugarLevels, 'createdAt')

      const sortedMorningSugarLevels = sortedSugarLevels.filter(({readingPeriod}) => readingPeriod.toLocaleLowerCase() === 'morning')
      const sortedAfternoonSugarLevels = sortedSugarLevels.filter(({readingPeriod}) => readingPeriod.toLocaleLowerCase() === 'afternoon')

      this.setState({
        morningSugarReading: sortedMorningSugarLevels.length > 0 ? sortedMorningSugarLevels[sortedMorningSugarLevels.length - 1].suger_reading : this.state.morningSugarReading,
        afternoonSugarReading: sortedMorningSugarLevels.length > 0 ? sortedAfternoonSugarLevels[sortedAfternoonSugarLevels.length - 1].suger_reading : this.state.afternoonSugarReading,
      })
    })
  }

  render() {
    return (
      <div className="blood-sugar-levels">
        <h2 className="title">Brent's Blood Sugar Levels</h2>
        <div className="levels">
          <div className="levels__block levels__block--morning">
            <span className="levels__value">
              <Counter end={this.state.morningSugarReading}/>
            </span>
            <span className="levels__period">Morning</span>
          </div>
          <div className="levels__block levels__block--night">
            <span className="levels__value">
              <Counter end={this.state.afternoonSugarReading}/>
            </span>
            <span className="levels__period">Night</span>
          </div>
        </div>
      </div>
    )
  }
}
