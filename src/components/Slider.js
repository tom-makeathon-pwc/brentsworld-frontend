import React, {Component} from 'react'
import {DateTime} from 'luxon'
import './Slider.css'

export default class Slider extends Component {
  render () {
    return (
      <div className="slider">
        {/*<button className="slider__nav slider__nav--prev">&lt;</button>*/}
        <div>
          <div className="slider__day">{new DateTime(new Date()).toFormat('EEEE')}</div>
          <div>{new DateTime(new Date()).toLocaleString(DateTime.DATE_FULL)}</div>
        </div>
        {/*<button className="slider__nav slider__nav--next">&gt;</button>*/}
      </div>
    )
  }
}
