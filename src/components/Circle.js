import React, {Component} from 'react'
import './Circle.css'

export default class Circle extends Component {
  render() {
    return (
      <div className={`c100 c100--thin p${this.props.percentage} big`}>
        <div className="content">{this.props.children}</div>
        <div className="slice">
          <div className="bar"/>
          <div className="fill"/>
        </div>
      </div>
    )
  }
}
