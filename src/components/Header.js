import React, {Component} from 'react'
import { Route, Switch } from 'react-router-dom'
import './Header.css'

export default class Header extends Component {
  render () {
    return (
      <header className="header">
        <div/>
        <div>
          <Switch>
            <Route path="/" exact render={() => 'Dashboard'}/>
            <Route path="/badges" exact render={() => 'Trophy Case'}/>
            <Route path="/prize-pond" exact render={() => 'Prize Pond'}/>
            <Route path="/prize-pond/reward/success" exact render={() => 'Prize Pond'}/>
            <Route path="/gym-routine" exact render={() => 'Gym Routine'}/>
            <Route path="/notepad" exact render={() => 'Notepad'}/>
            <Route render={() => ''}/>
          </Switch>
        </div>
        <div/>
      </header>
    )
  }
}
