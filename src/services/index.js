import axios from 'axios'

export const get = url => axios.get(url).then(({data}) => data)

export const post = (url, payload) => axios.post(url, payload).then(({data}) => data)

export const getSugarLevels = () => {
  return get('http://13.70.181.152:8080/api/sugerreadings')
}

export const getWeights = () => {
  return get('http://13.70.181.152:8080/api/readallweights')
}

export const postWeight = (payload) => {
  return post('http://13.70.181.152:8080/api/writeweight', payload)
}

export const getNotes = () => {
  return get('http://13.70.181.152:8080/api/notes')
}


export const postNote = (payload) => {
  return post('http://13.70.181.152:8080/api/notes', payload)
}

export const getSteps = () => {
  return get('http://13.70.181.152:8080/api/readsteps')
}

export const getFish = () => {
  return get('http://13.70.181.152:8080/api/getalltrophies')
}

export const postFish = (payload) => {
  return post('http://13.70.181.152:8080/api/createtrophy', payload)
}

