import React, {Component} from 'react'
import { Link, Redirect } from 'react-router-dom'
import './Badges.css'
import Fish from '../../icons/icon-fish.svg'
import { postFish } from '../../services/index'

export default class Badges extends Component {
  constructor (props) {
    super(props)

    this.state = {
      description: '',

      submitted: false,
    }
  }

  handleClick = event => {
    event.preventDefault()

    this.createFish()
  }

  handleSubmit = event => {
    event.preventDefault()

    this.createFish()
  }

  createFish = () => {
    postFish({
      description: this.state.description,
      fishIndex: 999,
    }).then(() => {
      this.setState({
        submitted: true,
      })
    })
  }

  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    })
  }

  render () {
    if (this.state.submitted) {
      return <Redirect to="/prize-pond/reward/success"/>
    }

    return (
      <div>
        <header className="header">
          <div><Link to="/prize-pond">&times;</Link></div>
          <div>Prize Pond</div>
          <div><button onClick={this.handleClick}>&#10004;</button></div>
        </header>

        <form className="badge-page" onSubmit={this.handleSubmit}>

          <div className="fish-pic">
            <img alt="Fish" src={Fish} className="fish-pic__image"/>
          </div>

          <div className="form-group">
            <label>Add A Comment</label>
            <input type="text" name="description" value={this.state.description} onChange={this.handleChange} required/>
          </div>

          <button className="button" type="submit">Send Brent a Fish</button>
        </form>
      </div>
    )
  }
}
