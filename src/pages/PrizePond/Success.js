import React, {Component} from 'react'
import { Link } from 'react-router-dom'
import './Success.css'

export default class Success extends Component {
  render () {
    return (
      <div className="reward-success-page">
        <div className="success__title">Sent!</div>

        <div className="success__description">Brent will receive the fish anywhere between 5-10 minutes.</div>

        <Link className="button" to="/">Back to dashboard</Link>
      </div>
    )
  }
}
