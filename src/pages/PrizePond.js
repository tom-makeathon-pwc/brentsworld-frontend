import React, {Component} from 'react'
import Slider from '../components/Slider'
import { Link } from 'react-router-dom'
import './PrizePond.css'
import { getFish } from '../services/index'
import {DateTime} from 'luxon'

export default class PrizePond extends Component {
  constructor (props) {
    super(props)

    this.state = {
      fishCount: 0,
      fishTotal: 8,
    }
  }

  componentDidMount() {
    getFish().then((fishos = []) => {
      const currentTime = DateTime.fromJSDate(new Date())

      const currentFishos = fishos.filter(({createdAt}) => {
        return DateTime.fromMillis(createdAt).hasSame(currentTime, 'month')
      })

      this.setState({
        fishCount: currentFishos.length >= this.state.fishTotal ? this.state.fishTotal : currentFishos.length
      })
    })
  }

  render() {
    return (
      <div>
        <Slider/>

        <div className="fish-count">
          <div>Brent has received</div>
          <div className="fish-count__indicator">
            <span className="fish-count__current">{this.state.fishCount}</span>
            <span>of</span>
            <span className="fish-count__total">8</span>
          </div>
          <div>fish this month</div>
        </div>

        {this.state.fishCount < this.state.fishTotal ? <Link className="button" to="/prize-pond/reward/new">Reward Brent</Link> : null}

      </div>
    )
  }
}
