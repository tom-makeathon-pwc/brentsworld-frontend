import React, {Component} from 'react'
import { Link, Redirect } from 'react-router-dom'
import {DateTime} from 'luxon'
import { postNote } from '../../services/index'

export default class Add extends Component {
  constructor (props) {
    super(props)

    this.state = {

      title: '',
      description: '',
      recordDate: `${new DateTime(new Date()).toISODate()}`,
      author: '',

      submitted: false,
    }
  }

  handleClick = event => {
    event.preventDefault()

    postNote({
      title: this.state.title,
      content: this.state.description,
      createdAt: this.state.recordDate,
      author: this.state.author,
    }).then(() => {
      this.setState({
        submitted: true,
      })
    })
  }

  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    })
  }

  render() {
    if (this.state.submitted) {
      return <Redirect to="/notepad"/>
    }

    return (
      <div>
        <header className="header">
          <div><Link to="/notepad">&times;</Link></div>
          <div>Add new</div>
          <div><button onClick={this.handleClick}>&#10004;</button></div>
        </header>

        <form className="weight-page" onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label>Enter Title</label>
            <input type="text" name="title" value={this.state.title} onChange={this.handleChange} required/>
          </div>

          <div className="form-group">
            <label>Description</label>
            <input type="text" name="description" value={this.state.description} onChange={this.handleChange} required/>
          </div>

          <div className="form-group">
            <label>Date</label>
            <input type="date" name="recordDate" value={this.state.recordDate} onChange={this.handleChange} required/>
          </div>

          <div className="form-group">
            <label>Your Name</label>
            <input type="text" name="author" value={this.state.author} onChange={this.handleChange} required/>
          </div>
        </form>
      </div>
    )
  }
}
