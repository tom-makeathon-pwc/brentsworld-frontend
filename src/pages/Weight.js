import React, {Component} from 'react'
import './Weight.css'
import {DateTime} from 'luxon'
import { Link, Redirect } from 'react-router-dom'
import { getWeights, postWeight } from '../services/index'
import {sortBy} from 'lodash'

export default class Weight extends Component {
  constructor (props) {
    super(props)

    this.state = {
      submitted: false,

      weight: 0,
      recordDate: `${new DateTime(new Date()).toISODate()}`,
    }
  }

  componentDidMount() {
    getWeights().then(weights => {
      const sortedWeights = sortBy(weights, 'createdAt')

      if (sortedWeights.length > 0) {
        this.setState({
          weight: sortedWeights[sortedWeights.length - 1].weight_reading
        })
      }
    })
  }

  handleSubmit = event => {
    event.preventDefault()

    this.submit(this.state.weight, this.state.recordDate)
  }

  handleClick = event => {
    event.preventDefault()

    this.submit(this.state.weight, this.state.recordDate)
  }

  submit = (weight, recordDate) => {
    postWeight({
      weight_reading: weight,
      createdAt: recordDate,
    }).then(() => {
      this.setState({
        submitted: true
      })
    })
  }

  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    })
  }

  render () {
    if (this.state.submitted) {
      return <Redirect to="/"/>
    }

    return (
      <div>
        <header className="header">
          <div><Link to="/">&times;</Link></div>
          <div>Add weight</div>
          <div><button onClick={this.handleClick}>&#10004;</button></div>
        </header>

        <form className="weight-page" onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label>Enter Weight (kg)</label>
            <input
              type="number"
              name="weight"
              min="1"
              max="200"
              required
              value={this.state.weight}
              onChange={this.handleChange}
            />
          </div>

          <div className="form-group">
            <label>Date</label>
            <input
              type="date"
              name="recordDate"
              value={this.state.recordDate}
              onChange={this.handleChange}
              required
            />
          </div>
        </form>
      </div>
    )
  }
}
