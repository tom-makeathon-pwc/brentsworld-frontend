import React, {Component} from 'react'
import BloodSugarLevels from '../components/BloodSugarLevels'
import './Dashboard.css'
import Circle from '../components/Circle'
import CountUp from 'react-countup'
import { Link } from 'react-router-dom'
import Slider from '../components/Slider'
import { getSteps, getWeights } from '../services/index'
import {sortBy} from 'lodash'

export default class Dashboard extends Component {
  constructor (props) {
    super(props)

    this.state = {
      steps: 0,
      totalSteps: 10000,
      weight: 0,
    }
  }

  componentDidMount() {
    getSteps().then((steps = []) => {
      const sortedSteps = sortBy(steps, 'createdAt')

      if (sortedSteps.length > 0) {
        this.setState({
          steps: sortedSteps[sortedSteps.length - 1].step_reading
        })
      }
    })

    getWeights().then((weights = []) => {
      const sortedWeights = sortBy(weights, 'createdAt')

      if (sortedWeights.length > 0) {
        this.setState({
          weight: sortedWeights[sortedWeights.length - 1].weight_reading
        })
      }
    })
  }

  render() {
    return (
      <div className="dashboard-page">
        <Slider/>

        <div className="step-counter">
          <Circle percentage={this.state.steps / this.state.totalSteps * 100}>
            <span>Brent<br/>completed</span>
            <span className="step-count">
              <CountUp
                end={this.state.steps}
                separator=","
                duration={1}
              />
            </span>
            <span>steps</span>
          </Circle>
        </div>

        <BloodSugarLevels/>

        <div className="weight-counter">
          <Circle percentage={40}>
            <span className="step-count">
              <CountUp
                end={this.state.weight}
                separator=","
                duration={1}
                decimals={1}
              />
            </span>
            <span>kgs</span>
          </Circle>
          <h2 className="title">Brent's Weight</h2>
          <Link className="button" to="/weight">Enter weight</Link>
        </div>
      </div>
    )
  }
}
