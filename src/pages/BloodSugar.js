import React, {Component} from 'react'

export default class BloodSugar extends Component {
  render() {
    return (
      <div>
        <form action="">

          <label>Date:</label>
          <input type="date" name="recordDate" required/>
            <br/>

            <label>Time Period:</label>
            <input type="radio" value="morning" name="timePeriod" checked/> Morning
              <input type="radio" value="afternoon" name="timePeriod"/> Afternoon<br/>

                <label>Blood Sugar Level:</label>
                <input type="number" name="bloodSugarLevel" min="-150" max="150" required/>mmol/L
                  <br/><br/>

                  <button type="submit">Save</button>
        </form>

        <hr />

        History:
        <table border="1" width="500">
          <tr>
            <th>Date</th>
            <th>Time Period</th>
            <th>Blood Sugar Level (mmol/L)</th>
          </tr>
        </table>
      </div>
    )
  }
}
