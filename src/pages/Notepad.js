import React, {Component} from 'react'
import { Link } from 'react-router-dom'
import './Notepad.css'
import {DateTime} from 'luxon'
import { getNotes } from '../services/index'

export default class Notepad extends Component {
  constructor (props) {
    super(props)

    this.state = {
      notes: []
    }
  }

  componentDidMount() {
    getNotes().then((notes = []) => {
      this.setState({
        notes: notes
      })
    })
  }

  render() {
    return (
      <div>
        <header className="header">
          <div/>
          <div>Notepad</div>
          <div><Link to="/notepad/add">+</Link></div>
        </header>

        <div className="notepad-page">
          {this.state.notes.map(({createdAt, title, content, author}, index) => (
            <div key={index} className="note">
              <div className="note__date">{new DateTime(createdAt).toLocaleString(DateTime.DATE_FULL)}</div>
              <div className="note__title">{title}</div>
              <div className="note__description">{content}</div>
              <div className="note__author">{author}</div>
            </div>
          ))}
        </div>
      </div>
    )
  }
}
