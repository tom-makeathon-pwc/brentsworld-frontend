import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route} from 'react-router-dom'
import Weight from './pages/Weight'
import BloodSugar from './pages/BloodSugar'
import Badges from './pages/PrizePond/Badges'
import Dashboard from './pages/Dashboard'
import Footer from './components/Footer'
import Notepad from './pages/Notepad'
import NotepadAdd from './pages/Notepad/Add'
import MainPageRoute from './components/MainPageRoute'
import PrizePond from './pages/PrizePond'
import Success from './pages/PrizePond/Success'

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <MainPageRoute path="/" exact component={Dashboard}/>
          <MainPageRoute path="/prize-pond" exact component={PrizePond}/>
          <Route path="/prize-pond/reward/new" component={Badges}/>
          <MainPageRoute path="/prize-pond/reward/success" component={Success}/>

          <Route path="/notepad" exact component={Notepad}/>
          <Route path="/notepad/add" component={NotepadAdd}/>

          <MainPageRoute path="/blood-sugar" component={BloodSugar}/>
          <Route path="/weight" component={Weight}/>

          <Footer/>
        </div>
      </Router>
    );
  }
}

export default App;
